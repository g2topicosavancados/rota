# Projeto Rota


## Disciplina
Tópicos Avançados em Banco de Dados


## Equipe
- Bruna Gonçalves
- Carlos Henrique Monteiro
- Douglas Nishiama
- Julio Rangel
- Leonardo Lins
- Renato Borges Gallo Junior
- Valdir Evaristo da Silva Junior

## Objetivo
Este projeto tem por objetivo criar um produto final que seja capaz de ajudar motoristas\
de vans a administrarem suas viagens, utilizando SCRUM em seu desenvolvimento.

## Tecnologias utilizadas
- React Native
- Nodejs
- MongoDB [Detalhes](https://gitlab.com/g2topicosavancados/leiame/-/wikis/MongoDB-Data-Schema)

### Entrega 1
[Detalhes](https://gitlab.com/g2topicosavancados/leiame/-/wikis/Sprint-1)
- Planejamento de arquitetura projeto
- Criação do database MongoDB
- Prototipar aplicação
- Função da API que cadastra destino do motorista
- Interface que cadastra destino

### Entrega 2
  [Detalhes](https://gitlab.com/g2topicosavancados/leiame/-/wikis/Sprint-2)
- Motorista gerenciar passageiro e consultar rota
- Passageiro pedir embarque/desembarque

### Entrega 3
[Detalhes](https://gitlab.com/g2topicosavancados/leiame/-/wikis/Sprint-3)
- Passageiro se inscrever em viagens
- Refatoração de estrutura de navegação front-end

### Entrega 4 
[Detalhes](https://gitlab.com/g2topicosavancados/leiame/-/wikis/Sprint-4)

##### Backend:
Incrementação das funções de cadastro / update:
- Inclusão do tipo de usuário na tela de cadastro.
- Inclusão da função de editar dados do passageiro.

Melhoria no sistema de comunicação motorista/passageiro:
- Implementação da função hora prevista, no ato de aceitar o passageiro o motorista inseri o horário previsto manualmente.

##### Frontend:

Incrementação das funções da trip usando a API:
- Implementação da função de cadastro de Passenger na trip.
- Criar e receber o código da trip.

Incrementação das funções do Passageiro usando a API:
-Listar os dados do perfil do passageiro.
-Listar as trips que o passageiro está inscrito.

### Entrega 5
[Detalhes](https://gitlab.com/g2topicosavancados/leiame/-/wikis/Sprint-5)
- Adicionando a API de geolocalização para melhorar o sistema de definição de rotas.
- Ordenação dos passageiros com base na API.
- Exclusão de passageiros da rota na viagem do dia.
- Realizar melhorias na estilização e animações de tela (Carregando login, buscando Viagem, e etc...).


### Entrega 6

[Detalhes](https://gitlab.com/g2topicosavancados/leiame/-/wikis/Sprint-5)
- Finalização das entregas referente a sprint 5
- Refatoração geral e revisão.



