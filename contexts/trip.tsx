import React, { createContext, useContext ,useState } from 'react';
import * as tripService from '../services/trip';
import { Alert } from 'react-native';
import AuthContext from './auth';

interface TripContextData {
  trip: object | null;
  createTrip( address: any, description: any,
          tripTime: Object ): Promise<void>;
}

const TripContext = createContext<TripContextData>({} as TripContextData);

export const TripProvider: React.FC = ({children}) => {
  const {user} = useContext(AuthContext);
  const [trip, setTrip] = useState<object | null>(null);

  async function createTrip(description: any, address: any,
                              tripTime: any) {
    const res = await tripService.createTrip(description, address, tripTime, user);
    setTrip(res);
    Alert.alert('Código da viagem: ', res.idTrip)
  }

  return(
    <TripContext.Provider value={{trip, createTrip}}>
      {children}
    </TripContext.Provider>
  );
}

export default TripContext;