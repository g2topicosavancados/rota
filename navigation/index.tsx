import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { ColorSchemeName } from 'react-native';

import { RootStackParamList } from '../types';
import BottomTabNavigator from './BottomTabNavigator';
import LinkingConfiguration from './LinkingConfiguration';
import SignIn from '../screens/SignIn';
import AuthContext, { AuthProvider } from '../contexts/auth';
import { TripProvider } from '../contexts/trip';

export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      //theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      theme={DarkTheme}>
      <AuthProvider>
        <TripProvider>
          <RootNavigator />
        </TripProvider>
      </AuthProvider>
    </NavigationContainer>
  );
}

const Stack = createStackNavigator<RootStackParamList>();

function RootNavigator() {
  const {signed} = React.useContext(AuthContext);

  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      {
        signed ? 
        <Stack.Screen name="Root" component={BottomTabNavigator} /> 
        : <Stack.Screen name="SignIn" component={SignIn} />
      }
    </Stack.Navigator>
  );
}
