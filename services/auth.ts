import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://192.168.15.19:5000/api',
  timeout: 3000,
});

interface Response {
  token: string;
  name: string
}

// interface Request {
//   email: string,
//   password: string
// }

export async function signIn(email: string, password: string): Promise<Response> {

  return await instance.post('/login', {email, password});
  // return new Promise((resolve) => {
  //   setTimeout(() => {
  //     resolve({
  //       token: 'nkw1963ieidjhbqwu9q0pj2lba190wond0i9d1u',
  //       name: 'Leo'
  //     })
  //   }, 2000)
  // })
}