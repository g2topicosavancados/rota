import axios from 'axios';

interface Response {
  idTrip: string;
}

// interface Request {
// }

export async function createTrip(description: string, address: string,
                                  tripTime: object, user: any):
                                   Promise<Response> {
  const instance = axios.create({
    baseURL: 'http://192.168.15.19:5000/api',
    timeout: 3000,
    headers: { Authorization: user.token }
  });
                                    
  return await instance.post('/trip', {description, address, tripTime});
  // return new Promise((resolve) => {
  //   setTimeout(() => {
  //     resolve({
  //       token: 'nkw1963ieidjhbqwu9q0pj2lba190wond0i9d1u',
  //       name: 'Leo'
  //     })
  //   }, 2000)
  // })
}