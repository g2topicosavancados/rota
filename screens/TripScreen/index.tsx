import React, { useState, useContext } from 'react';
import { StyleSheet, TextInput, Button } from 'react-native';
import MapView from 'react-native-maps';
import { Text, View } from '../../components/Themed';
import TripContext from '../../contexts/trip';

const TripScreen: React.FC = () => {
  const { createTrip } = useContext(TripContext);
  const [description, setDescription] = useState();
  const [address, setAddress] = useState();
  const [arriveTime, setArriveTime] = useState();
  const [leaveTime, setLeaveTime] = useState();

  function handleCreateTrip() {
    createTrip(description, address, { arriveTime, leaveTime } )
  }

  function onChangeDesc(data: any) {
    setDescription(data)
  }

  function onChangeAddress(data: any) {
    setAddress(data)
  }

  function onChangeArrival(data: any) {
    setArriveTime(data)
  }
  
  function onChangeLeave(data: any) {
    setLeaveTime(data)
  }

  return (
    <View style={styles.container}>
      <View style={styles.form}>
        <Text style={styles.span}>Descrição</Text>
        <TextInput style={styles.input} value={description} 
          onChangeText={text => onChangeDesc(text)}/>
        <Text style={styles.span}>Endereço</Text>
        <TextInput style={styles.input} value={address}
          onChangeText={text => onChangeAddress(text) }/>
        <View>
          <MapView style={styles.mapStyle} />
        </View>
        <View style={styles.timeInput}>
          <View style={styles.inputComponent}>
            <Text style={styles.span}>Chegada</Text>
            <TextInput style={styles.shortInput} value={arriveTime}
              onChangeText={text => onChangeArrival(text)}/>
          </View>
          <View style={styles.inputComponent}>
            <Text style={styles.span}>Partida</Text>
            <TextInput style={styles.shortInput} value={leaveTime} 
              onChangeText={text => onChangeLeave(text)}/>
          </View>
        </View>
        <Button title='Confirmar' onPress={handleCreateTrip}/>
        
      </View>
    </View>
  );
}   
        

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  form: {
    marginTop: 40
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 30,
    marginTop: 30,
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  inputComponent:{
    flexDirection: 'column',
    alignItems: 'center',
    marginRight: 10
  },
  input:{
    marginBottom: 20,
    color: 'white',
    width: 300,
    height: 40,
    borderBottomColor: 'white',
    borderWidth: 1
  },
  shortInput:{
    marginBottom: 20,
    color: 'white',
    width: 100,
    height: 40,
    borderBottomColor: 'white',
    borderWidth: 1
  },
  mediumInput:{
    marginBottom: 20,
    color: 'white',
    width: 150,
    height: 40,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomColor: 'white',
    borderWidth: 1
  },
  span:{
    fontSize: 12,
    fontWeight: 'bold',
  },
  timeInput:{
    flexDirection: 'row',
    justifyContent: 'center'
  },
  mapStyle: {
    height:200,
    width: 300,
    marginBottom: 20,
  },
});

export default TripScreen;