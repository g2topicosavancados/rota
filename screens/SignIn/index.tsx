import React, {useContext, useState} from 'react';
import { View, Button, StyleSheet, TextInput, Text, Dimensions, Image } from 'react-native';
import AuthContext from '../../contexts/auth';

const SignIn: React.FC = () => {
  const {signIn} = useContext(AuthContext);
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  function handleSignIn(){
    signIn(email, password);
  }

  function onChangeEmail(data: any) {
    setEmail(data);
  }

  function onChangePassword(data: any) {
    setPassword(data);
  }

  return(
    <>
      <View style={styles.header}>
        <Image
          style={styles.logo}
          source={require('./Logo.jpg')}
        />
      </View>
      <View style={styles.container}>
        <Text style={styles.span}>Email</Text>
        <TextInput style={styles.input} value={email}
          onChangeText={text => onChangeEmail(text)}></TextInput>
        <Text style={styles.span}>Senha</Text>
        <TextInput style={styles.input} value={password}
          onChangeText={text => onChangePassword(text)}></TextInput>
        <View style={styles.button}>
          <Button color="#404CCF" title='Entrar' onPress={handleSignIn}></Button>
        </View>
        <View style={styles.button}>
          <Button color="#404CCF" title='Register' onPress={handleSignIn}></Button>
        </View>
      </View>
    </>
  )
}

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 50,
    paddingRight: 50,
    backgroundColor: "#fff",
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#404CCF',
    width: width,
    height: height*0.3,
  },
  logo: {
    backgroundColor: '#404CCF',

  },
  input:{
    marginBottom: 20,
    color: 'white',
    borderBottomColor: '#323232',
    borderTopColor: '#fff',
    borderRightColor: '#fff',
    borderLeftColor: '#fff',
    borderWidth: 1, 
  },
  span:{
    color: '#323232',
    fontSize: 12,
    fontWeight: 'bold',
  },
  button:{
    paddingTop: 5,
    paddingBottom: 5,
  }
})

export default SignIn;